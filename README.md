BetaVsBeta_approximated.ipynb:

In this code we test the accuracy of the approximation used in chapter 4 of the thesis _Searching for Light Scalar Dark Matter with Gravitational Waves Detector_ for the modified Bessel function. See equations (4.126-4.130).


FDM Statistical Constraint Signal Analysis1.ipynb: 

In this code we simulate several data sets containing a representation of the expected signal generated by the interaction of ultra-light scalar filed dark matter with the optical apparatus of gravitational waves interferometers. The signal is then buried in Gaussian noise. The detection method discussed in chapter 4 of the thesis _Searching for Light Scalar Dark Matter with Gravitational Waves Detector_ is implemented and tested on the simulated data.


FDM Statistical constraints for Manuscript.ipynb:

In this code we evaluate in detail the signal generated by the interaction  of ultra-light scalar filed dark matter with the optical apparatus of gravitational waves interferometers as well as the expected constraint curves on the coupling parameter d_g^*.
Most of the plots that are present in chapter 4 of the thesis _Searching for Light Scalar Dark Matter with Gravitational Waves Detector_ are obtained in this code. 
